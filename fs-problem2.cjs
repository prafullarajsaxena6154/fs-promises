const fs = require('fs');

function readingFile(absolutePathOfRandomDirectory) {
    return new Promise((resolve, reject) => {

        fs.readFile(absolutePathOfRandomDirectory, (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                console.log("Reading file " + `${absolutePathOfRandomDirectory}`);
                resolve(data);
            }
        });
    });
};

function writingFile(absolutePathOfRandomDirectory, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(absolutePathOfRandomDirectory, data, (err) => {
            if (err) {
                reject(err);
            }
            else {
                console.log("Writing on file " + `${absolutePathOfRandomDirectory}`);
                resolve(absolutePathOfRandomDirectory);
            }
        })
    })
}

function appendingFile(absolutePathOfRandomDirectory, data) {
    return new Promise((resolve, reject) => {
        fs.appendFile(absolutePathOfRandomDirectory, data, (err) => {
            if (err) {
                reject(err);
            }
            else {
                console.log("Appending on file " + `${absolutePathOfRandomDirectory}`);
                resolve(absolutePathOfRandomDirectory);
            }
        })
    })
};

function deleteFiles(absolutePathOfRandomDirectory) {
    return new Promise((resolve, reject) => {
        fs.unlink((absolutePathOfRandomDirectory), (err) => {
            if (err) {
                reject(err);
            }
            else {
                console.log("Deleting file " + `${absolutePathOfRandomDirectory}`);
                resolve(absolutePathOfRandomDirectory);
            }
        })
    })
}


function fsProblem2(absolutePathOfRandomDirectory) {
    readingFile(absolutePathOfRandomDirectory).then((data) => {
        let dataUppercase = data;
        dataUppercase = dataUppercase.toString().toUpperCase();
        return writingFile('lipsumUpperCase.txt', JSON.stringify(dataUppercase,null,2));
    }).then((fileName) => {
        return writingFile('filenames.txt', fileName+'\n');
    }).then((fileName) => {
        return readingFile('lipsumUpperCase.txt');
    }).then((data) => {
        let dataLowerCase = data;
        dataLowerCase = dataLowerCase.toString().toLowerCase();
        dataLowerCase = dataLowerCase.split(/[.\n]+/);
        return writingFile('lipsumLowerCaseSentences.txt', JSON.stringify(dataLowerCase,null,2));
    }).then((fileName) => {
        return appendingFile('filenames.txt', fileName+'\n');
    }).then((fileName) => {
        return readingFile('lipsumLowerCaseSentences.txt');
    }).then((data) => {
        let sortedData = data.toString().split(' ');
        // sortedData = sortedData.toString();
        sortedData = sortedData.sort();
        return writingFile('lipsumLowerCaseSentencesSorted.txt', JSON.stringify(sortedData,null,2));
    }).then((fileName) => {
        return appendingFile('filenames.txt', fileName);
    }).then((fileName) => {
        return readingFile(fileName);
    }).then((data) => {
        fileNamesArray = data;
        fileNamesArray = fileNamesArray.toString();
        fileNamesArray = data.toString().split('\n');
        for (let index = 0; index < fileNamesArray.length; index++) {
            deleteFiles(fileNamesArray[index]);
        }
    }).catch((err) => {
        console.log(err);
    })
}

module.exports = fsProblem2;
const fs = require('fs');
const path = require('path');
// const filePath = path.join(__dirname, './callbacks');

function makeDirectory(absolutePathOfRandomDirectory) {
    // console.log("lol");
    return new Promise((resolve, reject) => {
        fs.mkdir(absolutePathOfRandomDirectory, (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve('Directory Created');
            }
        })
    });
};

function createRandomFile(absolutePathOfRandomDirectory, randomNumberOfFiles) {

    return new Promise((resolve, reject) => {
        let fileAddress = [];

        for (let index = 1; index <= randomNumberOfFiles; index++) {
            let characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            let randomContent = "";

            for (let index = 0; index < Math.random() * characters.length; index++) {
                randomContent += characters.substring(Math.random() * characters.length, characters.length + 2);
            }
            let fileName = "file_" + index;


            fs.writeFile(`${absolutePathOfRandomDirectory}/${fileName}`, randomContent, (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    fileAddress.push(`${absolutePathOfRandomDirectory}/${fileName}`);
                    console.log(("files " + fileName + " and data stored!"));
                    if (fileAddress.length === randomNumberOfFiles) {
                        resolve(fileAddress);
                    };
                }
            });
        }
    });
};

function deleteFiles(fileNames) {
    return new Promise((resolve, reject) => {
        for (let value of fileNames) {
            fs.unlink(value, (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve("Files deleted successfully!");
                }
            });
        }
    });
};


function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {
    // console.log("lol");
    makeDirectory(absolutePathOfRandomDirectory).then((obMakeDir) => {
        console.log(obMakeDir);
        return createRandomFile(absolutePathOfRandomDirectory, randomNumberOfFiles);

    }).then((fileAddress) => {
       return deleteFiles(fileAddress);
    }).then((deleteMessage) => {
        console.log(deleteMessage);
    }).catch((err) => {
        console.error(err)
    });
};
// let filePath = path.join(__dirname,'/callbacks');
// fsProblem1(filePath,8);

module.exports = fsProblem1;
